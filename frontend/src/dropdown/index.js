import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class DropDownComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            dropdownList: [],
            selected: "Select"
        }
    }
    componentDidMount() {
        if (this.props.list) {
            this.setState({
                dropdownList: this.props.list
            })
        }
    }

    toggle = () => {
        this.setState(prevState => ({
            open: !prevState.open,
        }));
    }

    handleSelect = (id, bucket) => {
        this.setState({
            selected: bucket
        }, () => {
            this.props.selected(id)
        })
    }


    render() {
        return (
            <Dropdown isOpen={this.state.open} toggle={this.toggle} className="w-100">
                <DropdownToggle caret>
                    {this.state.selected}
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem value="" onClick={() => this.handleSelect(null, "Select")}>Select</DropdownItem>
                    {this.state.dropdownList.map((val) => {
                        return <DropdownItem value={val._id}
                            onClick={() => this.handleSelect(val._id, val.bucket)}>{val.bucket}</DropdownItem>
                    })}
                </DropdownMenu>
            </Dropdown>
        );
    }
}

export default DropDownComponent;