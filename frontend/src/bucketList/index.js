import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'
import '../index.css'
import Modal from "../Modal"
import DropDownComponent from "../dropdown"
import {
    Button, Jumbotron, Card, CardText, CardBody,
    CardTitle, CardSubtitle, Col, Input, Row
} from "reactstrap"
class BucketList extends Component {
    constructor() {
        super();
        this.state = {
            modal: false,
            dropdown: false,
            bucketList: [],
            body: null,
            text: "",
            title: "",
            selectedBucket: null,
            currFunction: null,
            task: ""
        }
    }
    componentDidMount() {
        // console.log(this.props)
        axios.get(`http://localhost:9999/todo/list`)
            .then(res => {
                this.setState({
                    bucketList: res.data
                })
            })
    }
    toggleModal = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    addBuckets = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            body: <Input onChange={this.handleInputChange} placeholder="Bucket Name" />,
            title: "Add Bucket",
            currFunction: this.addBucketsDone
        }));
    }

    addBucketsDone = () => {
        axios.post(`http://localhost:9999/todo/addBucket`, { bucketName: this.state.text })
            .then(res => {
                this.setState(prevState => ({
                    modal: !prevState.modal,
                    bucketList: res.data
                }));
            })
    }

    handleInputChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }

    addTodo = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            body: <>
                <Input onChange={this.handleInputTaskChange} placeholder="Task Name" />
                <br />
                <p>Create Bucket or choose from existing bucket</p>
                <Input onChange={this.handleInputChange} placeholder="Create Bucket" />
                <br />
                <div className="d-flex">
                    <p>Bucket List:</p>
                    <DropDownComponent list={this.state.bucketList} selected={this.handleSelectedBucket} />
                </div>
            </>,
            title: "Add ToDo",
            currFunction: this.handleTodoDone
        }));
    }
    handleSelectedBucket = (id) => {
        this.setState({
            selectedBucket: id
        })
    }
    handleTodoDone = () => {
        let reqBody = {
            _id: this.state.selectedBucket,
            todo: this.state.task,
            bucketName: this.state.text
        }
        console.log(reqBody)
        axios.post(`http://localhost:9999/todo/addToDo`, reqBody)
            .then(res => {
                this.setState(prevState => ({
                    modal: !prevState.modal,
                    bucketList: res.data,
                    task: "",
                    text: "",
                    selectedBucket: null
                }));
            })
    }
    handleInputTaskChange = (e) => {
        this.setState({
            task: e.target.value
        })
    }

    render() {
        return (
            <>
                <Jumbotron>
                    <h1 className="display-3">Bucket List</h1>
                    <p className="lead">
                        <Button color="info" onClick={this.addBuckets}>Add Buckets</Button>
                        <Button color="primary" className="ml-3" onClick={this.addTodo}>Add ToDo</Button>
                    </p>
                </Jumbotron>
                <Row sm="12">
                    {this.state.bucketList.map((val) => {
                        return <Col sm="3" className="mt-2">
                            <Card key={val._id}>
                                <CardBody>
                                    <CardTitle>{val.bucket}</CardTitle>
                                    <CardSubtitle>Card subtitle</CardSubtitle>
                                    <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                    <Link to={`/bucket/${val._id}`}>View ToDos</Link>
                                </CardBody>
                            </Card>
                        </Col>
                    })}
                </Row>
                <Modal toggle={this.toggleModal}
                    modal={this.state.modal}
                    body={this.state.body}
                    title={this.state.title}
                    doneFunction={this.state.currFunction} />
            </>
        );
    }
}

export default BucketList;
