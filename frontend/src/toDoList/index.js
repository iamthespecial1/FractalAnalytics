import React from 'react';
import {
    Button, Card, CardText, CardBody,
    CardTitle, CardSubtitle, Jumbotron, Row, Input
} from "reactstrap"
import axios from 'axios';
import "./index.css"
import Modal from "../Modal"
class ToDoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todoList: [],
            bucketName: "",
            edit: false,
            text: null,
            selected: null,
            header: "",
            doneFunction: null
        }
    }

    toggle = () => {
        this.setState(prevState => ({
            edit: !prevState.edit,
            text: null
        }));
    }

    componentDidMount() {
        axios.get(`http://localhost:9999/todo/bucket/${this.props.match.params.id}`)
            .then(res => {
                this.setState({
                    todoList: res.data.todoList,
                    bucketName: res.data.bucket
                })
            })
    }

    handleToggleDone = (id) => {
        let reqBody = {
            bucketId: this.props.match.params.id,
            taskId: id
        }
        axios.put(`http://localhost:9999/todo/doneToggle`, reqBody)
            .then(res => {
                this.setState({
                    todoList: res.data.todoList,
                    bucketName: res.data.bucket
                })
            })
    }
    handleDelete = (id) => {
        let reqBody = {
            bucketId: this.props.match.params.id,
            taskId: id
        }
        axios.delete(`http://localhost:9999/todo/deleteTask`, {
            headers: {
                Authorization: null
            },
            data: {
                reqBody
            }
        })
            .then(res => {
                this.setState({
                    todoList: res.data.todoList,
                    bucketName: res.data.bucket
                })
            })
    }
    handleEdit = (id, text) => {
        this.setState(prevState => ({
            edit: !prevState.edit,
            text: text,
            selected: id,
            heaeder: "Edit Task",
            doneFunction: this.handleEditDone
        }));
    }
    handleInputChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }
    handleEditDone = () => {
        let reqBody = {
            bucketId: this.props.match.params.id,
            taskId: this.state.selected,
            todo: this.state.text
        }
        axios.put(`http://localhost:9999/todo/editTask`, reqBody)
            .then(res => {
                this.setState(prevState => ({
                    edit: !prevState.edit,
                    todoList: res.data.todoList,
                    bucketName: res.data.bucket,
                    text: null
                }));
            })
    }
    handleAdd = () => {
        this.setState(prevState => ({
            edit: !prevState.edit,
            header: "Add Task",
            doneFunction: this.handleAddDone
        }));
    }
    handleAddDone = () => {
        let reqBody = {
            _id: this.props.match.params.id,
            todo: this.state.text
        }
        axios.put(`http://localhost:9999/todo/addTask`, reqBody)
            .then(res => {
                this.setState(prevState => ({
                    edit: !prevState.edit,
                    todoList: res.data.todoList,
                    bucketName: res.data.bucket,
                    text: null
                }));
            })
    }


    render() {
        let input = <Input onChange={this.handleInputChange} placeholder="Task Name"
            value={this.state.text} />
        return (
            <div>
                <Jumbotron>
                    <h1 className="display-5">ToDo List</h1>
                    <p className="lead" className="display-4">
                        {this.state.bucketName}
                    </p>
                    <Button onClick={this.handleAdd}><i class="fa fa-plus"></i></Button>
                </Jumbotron>
                {this.state.todoList.map((val) => {
                    return <Card key={val._id} className="mt-2">
                        <CardBody>
                            <Row>
                                <span className={!val.done ? "fa fa-star-o" : "fa fa-star checked"}
                                    onClick={() => this.handleToggleDone(val._id)}></span>
                                <CardTitle className="ml-2">{val.todo}</CardTitle>
                            </Row>
                            <CardText>
                                <Button onClick={() => this.handleEdit(val._id, val.todo)}><i class="fa fa-pencil"></i></Button>
                                <Button className="ml-3" color="danger" onClick={() => this.handleDelete(val._id)}><i class="fa fa-times"></i></Button>
                            </CardText>

                        </CardBody>
                    </Card>
                })}
                <Modal toggle={this.toggle}
                    modal={this.state.edit}
                    body={input}
                    title={this.state.header}
                    doneFunction={this.state.doneFunction} />
            </div >
        );
    }
}

export default ToDoList;