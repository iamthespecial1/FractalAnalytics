import React from "react";
import { Route, Redirect } from "react-router-dom";
import App from "../App";
import BucketList from "../bucketList"
import ToDoList from "../toDoList"
class RouteComp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            url: null,
            component: null
        }
    }

    componentDidMount() {
    }
    render() {
        return (
            <React.Fragment>
                <Route exact path="/" component={App} />
                <Route exact path="/buckets" component={BucketList} />
                <Route exact path="/bucket/:id" component={ToDoList} />
            </React.Fragment>
        );
    }
}

export default RouteComp;