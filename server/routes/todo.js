var express = require('express');
var router = express.Router();
var uniqid = require('uniqid');
const ToDo = require('../model/todo');
/* GET users listing. */
router.get('/list', function (req, res, next) {
    ToDo.find({}, { bucket: 1, _id: 1 })
        .exec(function (err, buckets, next) {
            if (err)
                res.send(err);
            else {
                res.send(buckets)
            }
        });
});

router.get('/bucket/:id', function (req, res, next) {
    ToDo.findById({ _id: req.params.id }, {})
        .exec(function (err, buckets, next) {
            if (err)
                res.send(err);
            else {
                res.send(buckets)
            }
        });
});

router.post('/addBucket', function (req, res, next) {
    let todo = new ToDo();
    todo.bucket = req.body.bucketName;
    todo.todoList = []
    todo.save(function (err, res) {
        if (err) {
            res.send(err);
            res.json("failed to add");
        }
        else {
            ToDo.find({}, { bucket: 1, _id: 1 })
                .exec(function (err, buckets, next) {
                    if (err)
                        res.send(err);
                    else {
                        res.send(buckets)
                    }
                });
        }
    });

});

router.post('/addToDo', function (req, res, next) {
    let todoObj = {
        // _id: uniqid(),
        todo: req.body.todo,
        done: false
    };
    if (!req.body._id) {
        let todo = new ToDo();
        todo.bucket = req.body.bucketName;
        todo.todoList = [{ ...todoObj }]
        todo.save(function (err, res) {
            if (err) {
                res.send(err);
                res.json("failed to add");
            }
            else {
                ToDo.find({}, { bucket: 1, _id: 1 })
                    .exec(function (err, buckets, next) {
                        if (err)
                            res.send(err);
                        else {
                            res.send(buckets)
                        }
                    });
            }
        });
    }
    else {
        ToDo.findByIdAndUpdate(
            { _id: req.body._id },
            { $push: { "todoList": todoObj } },
            { safe: true, upsert: true, new: true },
            function (err, model) {
                if (err) {
                    console.log(err);
                    return res.send(err);
                }
                else {
                    ToDo.find({}, { bucket: 1, _id: 1 })
                        .exec(function (err, buckets, next) {
                            if (err)
                                res.send(err);
                            else {
                                res.send(buckets)
                            }
                        });
                }
            });
    }


});

router.put('/addTask', function (req, res, next) {
    console.log(req.body)
    let todoObj = {
        // _id: uniqid(),
        todo: req.body.todo,
        done: false
    };
    ToDo.findByIdAndUpdate(
        { _id: req.body._id },
        { $push: { "todoList": todoObj } },
        { safe: true, upsert: true, new: true },
        function (err, model) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            else {
                ToDo.findById({ _id: req.body._id }, {})
                    .exec(function (err, bucket, next) {
                        if (err)
                            res.send(err);
                        else {
                            res.send(bucket)
                        }
                    });
            }
        });



});
router.put('/doneToggle', function (req, res, next) {
    console.log(req.body)
    ToDo.findById({ _id: req.body.bucketId })
        .exec(function (err, buckets, next) {
            if (err)
                res.send(err);
            else {

                buckets.todoList.forEach(element => {
                    console.log(element._id)
                    if (element._id == req.body.taskId) {
                        element.done = !element.done
                    }
                });

                buckets.save(function (err, blog) {

                    if (err) {
                        res.send(err);
                        res.json("failed to add reply");
                    }
                    else {
                        res.json(blog);
                    }
                });
            }
        });
});

router.delete('/deleteTask', function (req, res, next) {
    const { reqBody } = req.body
    console.log(reqBody)
    ToDo.findByIdAndUpdate(
        reqBody.bucketId,
        { $pull: { todoList: { _id: reqBody.taskId } } }, { new: true }, function (err, model) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.json(model);
        });
});

router.put('/editTask', function (req, res, next) {
    ToDo.update(
        { "_id": req.body.bucketId, "todoList._id": req.body.taskId },
        { $set: { "todoList.$.todo": req.body.todo } }, { new: true },
        function (err, updated) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            else {
                ToDo.findById({ _id: req.body.bucketId }, {})
                    .exec(function (err, buckets, next) {
                        if (err)
                            res.send(err);
                        else {
                            res.send(buckets)
                        }
                    });
            }
        })

});


module.exports = router;
