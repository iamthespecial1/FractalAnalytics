const mongoose = require('mongoose');
var currentdate = new Date();
var d = currentdate.getDate() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getFullYear()

// User Schema
const toDoSchema = mongoose.Schema({
    bucket: {
        type: String,
        required: true
    },
    todoList: {
        type: [{
            done: Boolean,
            todo: String,
            id: String
        }],
        default: []
    },

}, { usePushEach: true });

const todo = module.exports = mongoose.model('todo', toDoSchema, 'todo');
